<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Recipe extends Facade
{
    /**
     * Create the Facade
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Recipe'; }
}