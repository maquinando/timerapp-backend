<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Preparation extends Facade
{
    /**
     * Create the Facade
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Preparation'; }
}