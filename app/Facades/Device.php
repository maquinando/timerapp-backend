<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Device extends Facade
{
    /**
     * Create the Facade
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Device'; }
}