<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Image as ImageModel;
use Image;

class PagesController extends Controller
{
    /**
     * Homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('mobile-app');
    }

    /**
     * Dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('dashboard.main');
    }


    public function showImage(Request $request, $image_id, $format = 'preview')
    {
        $imgObject = ImageModel::find($image_id);
        if(!empty($imgObject)) {
            $img = Image::make($imgObject->image);
            switch ($format) {
                case 'preview':
                    $img->fit(540, 300);
                    break;
                case 'thumbnail':
                    $img->fit(150, 100);
                    break;
                default:
                    break;
            }
            
            return $img->response('jpg', 70);
        }
        $payload = $request->all();
        return json_encode(compact('payload', 'imgObject', 'format'));
    }
}
