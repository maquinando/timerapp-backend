<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ImageService;
use App\Http\Requests\ImageCreateRequest;
use App\Http\Requests\ImageUpdateRequest;

class ImagesController extends Controller
{
    public function __construct(ImageService $imageService)
    {
        $this->service = $imageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $images = $this->service->paginated();
        return view('images.index')->with('images', $images);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $images = $this->service->search($request->search);
        return view('images.index')->with('images', $images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ImageCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('images.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('images.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the image.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = $this->service->find($id);
        return view('images.show')->with('image', $image);
    }

    /**
     * Show the form for editing the image.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = $this->service->find($id);
        return view('images.edit')->with('image', $image);
    }

    /**
     * Update the images in storage.
     *
     * @param  \Illuminate\Http\ImageUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImageUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the images from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('images.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('images.index'))->with('message', 'Failed to delete');
    }
}
