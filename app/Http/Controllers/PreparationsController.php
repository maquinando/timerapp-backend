<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PreparationService;
use App\Http\Requests\PreparationCreateRequest;
use App\Http\Requests\PreparationUpdateRequest;

class PreparationsController extends Controller
{
    public function __construct(PreparationService $preparationService)
    {
        $this->service = $preparationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $preparations = $this->service->paginated();
        return view('preparations.index')->with('preparations', $preparations);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $preparations = $this->service->search($request->search);
        return view('preparations.index')->with('preparations', $preparations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('preparations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\PreparationCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PreparationCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('preparations.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('preparations.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the preparation.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $preparation = $this->service->find($id);
        return view('preparations.show')->with('preparation', $preparation);
    }

    /**
     * Show the form for editing the preparation.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $preparation = $this->service->find($id);
        return view('preparations.edit')->with('preparation', $preparation);
    }

    /**
     * Update the preparations in storage.
     *
     * @param  \Illuminate\Http\PreparationUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PreparationUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the preparations from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('preparations.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('preparations.index'))->with('message', 'Failed to delete');
    }
}
