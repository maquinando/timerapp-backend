<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StepService;
use App\Http\Requests\StepCreateRequest;
use App\Http\Requests\StepUpdateRequest;

class StepsController extends Controller
{
    public function __construct(StepService $stepService)
    {
        $this->service = $stepService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $steps = $this->service->paginated();
        return view('steps.index')->with('steps', $steps);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $steps = $this->service->search($request->search);
        return view('steps.index')->with('steps', $steps);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('steps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StepCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StepCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('steps.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('steps.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the step.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $step = $this->service->find($id);
        return view('steps.show')->with('step', $step);
    }

    /**
     * Show the form for editing the step.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $step = $this->service->find($id);
        return view('steps.edit')->with('step', $step);
    }

    /**
     * Update the steps in storage.
     *
     * @param  \Illuminate\Http\StepUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StepUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the steps from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('steps.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('steps.index'))->with('message', 'Failed to delete');
    }
}
