<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\IngredientService;
use App\Http\Requests\IngredientCreateRequest;
use App\Http\Requests\IngredientUpdateRequest;

class IngredientsController extends Controller
{
    public function __construct(IngredientService $ingredientService)
    {
        $this->service = $ingredientService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ingredients = $this->service->paginated();
        return view('ingredients.index')->with('ingredients', $ingredients);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $ingredients = $this->service->search($request->search);
        return view('ingredients.index')->with('ingredients', $ingredients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ingredients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\IngredientCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngredientCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('ingredients.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('ingredients.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the ingredient.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingredient = $this->service->find($id);
        return view('ingredients.show')->with('ingredient', $ingredient);
    }

    /**
     * Show the form for editing the ingredient.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingredient = $this->service->find($id);
        return view('ingredients.edit')->with('ingredient', $ingredient);
    }

    /**
     * Update the ingredients in storage.
     *
     * @param  \Illuminate\Http\IngredientUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IngredientUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the ingredients from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('ingredients.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('ingredients.index'))->with('message', 'Failed to delete');
    }
}
