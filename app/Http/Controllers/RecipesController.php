<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RecipeService;
use App\Http\Requests\RecipeCreateRequest;
use App\Http\Requests\RecipeUpdateRequest;
use Image;
use App\Models\Recipe;
use App\Models\Image as ImageModel;

class RecipesController extends Controller
{
    public function __construct(RecipeService $recipeService)
    {
        $this->service = $recipeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recipes = $this->service->paginated();
        return view('recipes.index')->with('recipes', $recipes);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $recipes = $this->service->search($request->search);
        return view('recipes.index')->with('recipes', $recipes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recipes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RecipeCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('recipes.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('recipes.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the recipe.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = $this->service->find($id);
        return view('recipes.show')->with('recipe', $recipe);
    }

    /**
     * Show the form for editing the recipe.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipe = $this->service->find($id);
        return view('recipes.edit')->with('recipe', $recipe);
    }

    /**
     * Update the recipes in storage.
     *
     * @param  \Illuminate\Http\RecipeUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeUpdateRequest $request, $id)
    {
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $img = Image::make($file);
            // $img->fit('564',' 364');
            $image_name = 'uploads/' . time()."-".$file->getClientOriginalName();
            $img->save($image_name, 80);

            $img_model = new ImageModel();
            $img_model->image = $image_name;
            $img_model->save();
            if($img_model->id) {
                $recipe = Recipe::find($id);
                $recipe->image()->associate($img_model);
                $recipe->save();
            }
        }
        
        $result = $this->service->update($id, $request->except('_token', 'image'));
        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the recipes from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('recipes.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('recipes.index'))->with('message', 'Failed to delete');
    }
}
