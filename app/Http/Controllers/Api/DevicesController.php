<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DeviceService;
use App\Http\Requests\DeviceCreateRequest;
use App\Http\Requests\DeviceUpdateRequest;
use App\Models\Device;

class DevicesController extends Controller
{
    public function __construct(DeviceService $deviceService)
    {
        $this->service = $deviceService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $devices = $this->service->paginated();
        return response()->json($devices);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $devices = $this->service->search($request->search);
        return response()->json($devices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\DeviceCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeviceCreateRequest $request)
    {
        return response()->json(['foo' => 'bar']);

        $result = $this->service->create($request->except('_token'));
        
        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to create device'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = $this->service->find($id);
        return response()->json($device);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\DeviceUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeviceUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to update device'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return response()->json(['success' => 'device was deleted'], 200);
        }

        return response()->json(['error' => 'Unable to delete device'], 500);
    }


    public function register(Request $request)
    {

        $data = $request->only('devicetoken', 'name', 'platforms', 'user_id');

        $result = $this->service->create($data);
        
        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to create device'], 500);
    }
}
