<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PreparationService;
use App\Http\Requests\PreparationRequest;

class PreparationsController extends Controller
{
    public function __construct(PreparationService $preparationService)
    {
        $this->service = $preparationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $preparations = $this->service->paginated();
        return response()->json($preparations);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $preparations = $this->service->search($request->search);
        return response()->json($preparations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\PreparationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PreparationRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to create preparation'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $preparation = $this->service->find($id);
        return response()->json($preparation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\PreparationRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PreparationRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to update preparation'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return response()->json(['success' => 'preparation was deleted'], 200);
        }

        return response()->json(['error' => 'Unable to delete preparation'], 500);
    }
}
