<?php

namespace App\Http\Controllers\Api;

use DB;
use Validator;
use Socialite;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Login a user
     *
     * @param  Request $request
     * @return JSON
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }
    /**
     * Login a user using the social data
     *
     * @param  Request $request
     * @return JSON
     */
    public function socialLogin(Request $request) {
        $provider = $request->provider;
        $token = $request->token;
        $socialUser = Socialite::driver($provider)->stateless()->userFromToken($token);
        $localUser = $this->service->findByEmail($socialUser->getEmail());
        if (empty($localUser)) {
            $localUser = User::where('name', $socialUser->getName())->first();
        }
        if (empty($localUser)) {
            $email = $socialUser->getEmail();
            if(empty($email)) {
                $email = 'ingresa_tu@correo.com';
            }
            $name = $socialUser->getName();
            $meta = array(
                'firstname' => $name
             );
            $nameArr = explode(" ", $name);
            if(count('name') == 2) {
                $meta['firstname'] = $nameArr[0];
                $meta['lastname'] = $nameArr[1];
            } elseif(count('name') == 4) {
                $meta['firstname'] = $nameArr[0].' '.$nameArr[1];
                $meta['lastname'] = $nameArr[2].' '.$nameArr[3];
            }
            
            $data = [
                'name' => $name,
                'email' => $email,
                'password' => bcrypt(md5(rand(222222, 999999)))
            ];
            $localUser = User::create($data);
            $this->service->create($localUser, $data['password'], 'member', true, $meta);
        }
        

        try {
            if(! $token = JWTAuth::fromUser($localUser))  {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token', 'localUser'));

    }


    /**
     * Refresh the token
     *
     * @return JSON
     */
    public function refresh()
    {
        $newToken = JWTAuth::parseToken()->refresh();
        return response()->json(compact('newToken'));
    }

    /**
     * Register a User
     *
     * @param  Request $request
     * @return JSON
     */
    public function register(Request $request)
    {
        $data = $request->only('name','email', 'password');

        $_user = User::where('email', $data['email'])->first();
        if(!empty($_user->id)) {
            return response()->json('user already registered', 409);
        }

        $meta = $request->only('firstname','lastname', 'birthday', 'country', 'gender');
        return DB::transaction(function() use ($data, $meta) {
            $user = User::create([
                'name' => empty($data['name'])? '': $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);

            $user = $this->service->create($user, $data['password'], 'member', true, $meta);

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('token'));
        });
    }

    public function recover(Reques $request) {

        $data = $request->only('email');
        $_user = User::where('email', $data['email'])->first();
        if(empty($_user->id)) {
            return response()->json('User doesn\'t exist.', 409);
        }
    }

    /**
     * Login a user using the social data
     *
     * @param  Request $request
     * @return JSON
     */
    public function socialRegister(Request $request) {
        $data = $request->only('email', 'password');

        return DB::transaction(function() use ($data) {
            $user = User::create([
                'name' => $data['email'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);

            $user = $this->service->create($user, $data['password']);
            $token = JWTAuth::fromUser($user);

            return response()->json(compact('token'));
        });

    }

}
