<?php

namespace App\Http\Controllers\Api;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RecipeService;
use App\Http\Requests\RecipeUpdateRequest;
use App\Http\Requests\RecipeCreateRequest;

class RecipesController extends Controller
{
    public function __construct(RecipeService $recipeService)
    {
        $this->service = $recipeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $filters = json_decode($request->filters);
        $recipes = $this->service->paginated($filters);
        foreach ($recipes as $key => $value) {
            $value->image = $value->image;
        }
        return response()->json($recipes);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $recipes = $this->service->search($request->search);
        return response()->json($recipes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RecipeCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to create recipe'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipe = $this->service->find($id);
        return response()->json($recipe);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\RecipeUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return response()->json($result);
        }

        return response()->json(['error' => 'Unable to update recipe'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return response()->json(['success' => 'recipe was deleted'], 200);
        }

        return response()->json(['error' => 'Unable to delete recipe'], 500);
    }

    public function getFilters(Request $request) {
        $categories = DB::table('categories')
        ->join('recipe_category', 'categories.id', '=', 'recipe_category.category_id')
        ->select('categories.id', 'categories.slug', 'categories.name')
        ->distinct()->get();
        
        $minDuration = DB::table('recipes')->min('preparation_time');
        $maxDuration = DB::table('recipes')->max('preparation_time');

        $minCalories = DB::table('recipes')->min('calories');
        $maxCalories = DB::table('recipes')->max('calories');
        
        $filters = array(
            'ranges' => array(
                'calories' => array(
                    'min' => intval($minCalories),
                    'max' => intval($maxCalories)
                ),
                'duration' => array(
                    'min' => intval($minDuration),
                    'max' => intval($maxDuration)
                )
            ),
            'categories' => $categories
        );
        return $filters;
    }
}
