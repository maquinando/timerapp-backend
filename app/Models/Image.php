<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $table = "images";

    public $primaryKey = "id";

    public $timestamps = false;

    public $fillable = [
        'id',
		'image',
		'thumbnail',

    ];

    public function recipes() {

    }

    public static $rules = [
        // create rules
    ];

    // Image 

}
