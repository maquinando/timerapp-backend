<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public $table = "recipes";

    public $primaryKey = "id";

    public $timestamps = true;

    protected $hidden = array( 'created_at', 'updated_at', 'location_id' );

    public $fillable = [
        'id',
        'name',
        // 'slug',
        'content',
        'image',
        'portions',
        'calories',
        'preparation_time',
        'created_at',
        'updated_at',
        'location'
        ];

    public static $rules = [
        'name' => 'required|max:128',
        // 'content' => 'required',
        // 'portions' => 'required',
        // 'calories' => 'required',
        'preparation_time' => 'required', 
        'image' => 'image'
    ];

    // Relationships
    public function categories() {
        return $this->belongsToMany('App\Models\Category', 'recipe_category');
    }
    // Relationships
    public function image() {
        return $this->belongsTo('App\Models\Image', 'preview');
    }


    public function preparations() {
        return $this->hasMany('App\Models\Preparation');
    } 

    public function location() {
        return $this->belongsTo('App\Models\Location');
    }


    public function save(array $options = [])
    {
        if( empty($this->slug) ) {
            $this->setSlugAttribute();
        }
        parent::save();
    }
    // Here I want to auto generate slug based on the title
    public function setSlugAttribute(){
        $this->attributes['slug'] = str_slug($this->name , "-");
    }
    

    
}
