<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_profile';
    
    /**
     * User
     *
     * @return Relationship
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
