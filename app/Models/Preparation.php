<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Preparation extends Model
{
    public $table = "preparations";
    //
    public static $rules = [
        'name' => 'required|min:4',
        'content' => 'max:128',
        'slug' => 'max:30',
        'recipe_id' => 'required'
    ];
    public $fillable = [
        'id',
        'name',
        'slug',
        'content',
        'recipe_id'
        ];

    public function recipe() {
        return $this->belongsTo('App\Models\Recipe');
    }
    public function steps() {
        return $this->hasMany('App\Models\Step');
    }
    public function ingredients() {
        return $this->hasMany('App\Models\Ingredient');
    } 

    public function save(array $options = [])
    {
        if( empty($this->slug) ) {
            $this->setSlugAttribute();
        }
        if( empty($this->content) ) {
            $this->content = $this->name;
        }
        parent::save();
    }
    // Here I want to auto generate slug based on the title
    public function setSlugAttribute(){
        $this->attributes['slug'] = str_slug($this->name , "-");
    }
}
