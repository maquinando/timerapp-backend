<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name', 'slug'];
    
    public $timestamps = false;

    public static $rules = [
        'name' => 'required|max:128'
    ];

    public function receipes() {
        return $this->belongsToMany('App\Models\Recipe', 'recipe_category');
    }

    public function save(array $options = [])
    {
        if( empty($this->slug) ) {
            $this->setSlugAttribute();
        }
        parent::save();
    }
}
