<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Device extends Model
{
    use SoftDeletes;
    public $table = "devices";

    public $primaryKey = "id";

    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public $fillable = [
    'id',
    'user_id',
    'token',
    'name',
    'platforms',
    'deleted_at',
    'created_at',
    'updated_at',

    ];

    public static $rules = array(
        'token' => 'required|unique:devices',
        'name' => 'required',
        'platforms' => 'required',
    );


    /**
     * Get the user that owns the device.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
