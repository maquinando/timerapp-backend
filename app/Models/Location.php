<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table = "locations";
    protected $fillable = array("code", "name", "image_id");
    protected $hidden = array("image_id"); 

    public static $rules = [
        'name' => 'required|max:128',
        'code' => 'required|max:4', 
    ];

    public $primaryKey = "id";


}
