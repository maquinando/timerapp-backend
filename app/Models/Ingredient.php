<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = "ingredients";
    public static $rules = [
        'quantity' => 'required|max:10',
        'name' => 'required|min:2',
        'slug' => 'max:30',
    ];
    public $fillable = [
        'id',
        'name',
        'slug',
        'quantity',
        'preparation_id',
        'units_id'
        ];

    public function units() {
        return $this->belongsTo('App\Models\Unit');
    }
    public function preparation() {
        return $this->belongsTo('App\Models\Preparation');
    }

    public function save(array $options = [])
    {
        // if( empty($this->slug) ) {
            $this->setSlugAttribute();
        // }
        parent::save();
    }
    // Here I want to auto generate slug based on the title
    public function setSlugAttribute(){
        $this->attributes['slug'] = str_slug($this->name , "-");
    }
}
