<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $table = 'steps';

    public static $rules = [
        'content' => 'required',
        'slug' => 'max:30',
    ];
    public $fillable = [
        'id',
        'content',
        'slug',
        'preparation_id'
        ];

    public function preparation() {
        return $this->belongsTo('App\Models\Preparation');
    }
}
