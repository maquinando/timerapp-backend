<?php

namespace App\Services;

use App\Models\Recipe;
use Illuminate\Support\Facades\Schema;

class RecipeService
{
    /**
     * Service Model
     *
     * @var Model
     */
    public $model;

    /**
     * Pagination
     *
     * @var integer
     */
    public $pagination;

    /**
     * Service Constructor
     *
     * @param Recipe $recipe
     */
    public function __construct(Recipe $recipe)
    {
        $this->model        = $recipe;
        $this->pagination   = env('PAGINATION', 25);
    }

    /**
     * All Model Items
     *
     * @return array
     */
    public function all()
    {
        $query = $this->model
            ->orderBy('updated_at', 'desc')
            ->where('preview', '>', 0);
        return $query->get();
    }

    /**
     * Paginated items
     *
     * @return LengthAwarePaginator
     */
    public function paginated($filters = null)
    {
        $query = $this->model
            ->orderBy('updated_at', 'desc')
            ->where('preview', '>', 0);
        if(!empty($filters)) {
            if(isset($filters->maxDuration)) {
                $query->where('preparation_time', '<=', $filters->maxDuration);
            }
            if(isset($filters->maxCalories)) {
                $query->where('calories', '<=', $filters->maxCalories);
            }
            if(isset($filters->categories)) {
                $categories = array();
                foreach ($filters->categories as $key => $value) {
                    if($value == true) {
                        array_push($categories, $key);
                    }
                }
                $query->join('recipe_category', 'recipes.id', '=', 'recipe_category.recipe_id')
                ->select('recipes.*')
                ->whereIn('recipe_category.category_id', $categories)
                ->distinct();
            }
            return $query->paginate($this->pagination);
            
        }
        return $query->paginate($this->pagination);
    }

    /**
     * Search the model
     *
     * @param  mixed $payload
     * @return LengthAwarePaginator
     */
    public function search($payload)
    {
        $query = $this->model->orderBy('created_at', 'desc');
        $query->where('id', 'LIKE', '%'.$payload.'%');

        $columns = Schema::getColumnListing('recipes');

        foreach ($columns as $attribute) {
            $query->orWhere($attribute, 'LIKE', '%'.$payload.'%');
        };

        return $query->paginate($this->pagination)->appends([
            'search' => $payload
        ]);
    }

    /**
     * Create the model item
     *
     * @param  array $payload
     * @return Model
     */
    public function create($payload)
    {
        return $this->model->create($payload);
    }

    /**
     * Find Model by ID
     *
     * @param  integer $id
     * @return Model
     */
    public function find($id)
    {
        $recipe = $this->model->find($id);
        $recipe->image;
        foreach($recipe->preparations as $preparation) {
            $preparation->steps;
            foreach($preparation->ingredients as $ingredient) {
                $ingredient->units;
            }
        }
        $recipe->categories;
        return $recipe;
    }

    /**
     * Model update
     *
     * @param  integer $id
     * @param  array $payload
     * @return Model
     */
    public function update($id, $payload)
    {
        if( isset($payload['categories']) ) {
            $this->find($id)->categories()->sync($payload['categories']);
            unset($payload['categories']);
        }
        return $this->find($id)->update($payload);
    }

    /**
     * Destroy the model
     *
     * @param  integer $id
     * @return bool
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }
}
