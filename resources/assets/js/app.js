
// Construct path to file upload route
// Useful if your dev and prod URLs are different
var path = CKEDITOR.basePath.split('/');
path[ path.length-2 ] = 'upload_image';
config.filebrowserUploadUrl = path.join('/').replace(/\/+$/, '');

// Add plugin
config.extraPlugins = 'filebrowser';



$(function(){


    $('.alert').delay(7000).fadeOut();

});