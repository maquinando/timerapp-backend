
<div class="container">
    <div class="row">
        @if(count($receipes) == 0) 
            <p>No hay recetas para mostrar</p>
        @else
            @foreach ($receipes as $receipe)
                <p>Name: {{ $receipe->name }}</p>
                <p>Slug: {{ $receipe->slug }}</p>
                <p>Content: {{ $receipe->Content }}</p>
            @endforeach
        @endif

    </div>

    <div>
        <h1> Crear recetas</h1>
        <form action="{{url('admin/receipe')}}" method="post" accept-charset="utf-8">
            {{ csrf_field() }}
            <div>
            <input type="text" name="name" placeholder="Name">
            </div>
            <div><input type="text" name="slug" placeholder="Slug"></div>
            
            <div><textarea name="content" placeholder="Content"></textarea></div>

            <div><input type="submit" value="Crear receta"></div>
        </form>
    </div>
</div>

