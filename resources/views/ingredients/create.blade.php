@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Create'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'ingredients.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Ingredients: Create</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['route' => 'ingredients.store']) !!}
            <?php 
            $columns = FormMaker::getTableColumns('ingredients');
            $preparation_id = @$_GET['preparation'] || '';
            unset($columns['preparation_id']);
            unset($columns['quantity']);
            unset($columns['units_id']);

            $allPreparations = \App\Models\Preparation::all();
            $allUnits = \App\Models\Unit::all();

             ?>
            @form_maker_object(null, $columns)
        
            {!! Form::label('Preparación') !!}
            <div class="" style="margin-bottom: 20px;">
                <select id="recipe-selector" class="selectpicker form-control" data-live-search="true" name="preparation_id" >
                    @foreach ($allPreparations as $preparation)
                        <option data-tokens="{{ $preparation->name }}" value="{{ $preparation->id }}" <?php echo ($preparation->id == $preparation_id)? 'selected':''?>>
                            {{ $preparation->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            {!! Form::label('Cantidad') !!}
            <div class="input-group">
                <input type="text" class="form-control" aria-label="..." name="quantity" value="">
                <div class="input-group-btn">
                    <select id="recipe-selector" class="btn selectpicker form-control" data-live-search="false" data-dropdown-align-right="true" name="units_id" >
                    @foreach ($allUnits as $unit)
                        <option data-tokens="{{ $unit->id }}" value="{{ $unit->id }}" >
                            {{ $unit->name }}
                        </option>
                    @endforeach
                    </select>
                </div><!-- /btn-group -->
            </div><!-- /input-group -->

            <div class="clearfix" style="margin-top: 20px;">
            {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
            </div>
            
            {!! Form::close() !!}

        </div>
    </div>

@stop