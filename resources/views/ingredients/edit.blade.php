@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Edit'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'ingredients.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Ingredients: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('ingredients.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {!! Form::model($ingredient, ['route' => ['ingredients.update', $ingredient->id], 'method' => 'patch']) !!}
            <?php 
            $columns = FormMaker::getTableColumns('ingredients');
            unset($columns['preparation_id']);
            unset($columns['quantity']);
            unset($columns['units_id']);

            $allPreparations = \App\Models\Preparation::all();
            $allUnits = \App\Models\Unit::all();

             ?>
            @form_maker_object($ingredient, $columns)

            {!! Form::label('Preparación') !!}
            <select id="recipe-selector" class="selectpicker form-control" data-live-search="true" name="preparation_id" >
                @foreach ($allPreparations as $preparation)
                    <option data-tokens="{{ $preparation->name }}" value="{{ $preparation->id }}" <?php echo ($ingredient->preparation->id == $preparation->id)? 'selected':''?> >
                        {{ $preparation->name }}
                    </option>
                @endforeach
            </select>
            {!! Form::label('Cantidad') !!}
            <div class="input-group">
                <input type="text" class="form-control" aria-label="..." name="quantity" value="{{$ingredient->quantity}}">
                <div class="input-group-btn">
                    <select id="recipe-selector" class="btn selectpicker form-control" data-live-search="false" data-dropdown-align-right="true" name="units_id" >
                    @foreach ($allUnits as $unit)
                        <option data-tokens="{{ $unit->id }}" value="{{ $unit->id }}" <?php echo ($ingredient->units && $ingredient->units->id == $unit->id)? 'selected':''?> >
                            {{ $unit->name }}
                        </option>
                    @endforeach
                    </select>
                </div><!-- /btn-group -->
            </div><!-- /input-group -->

            {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@stop
