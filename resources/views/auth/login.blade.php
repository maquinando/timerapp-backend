@extends('layouts.master')

@section('app-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <h1 class="text-center">Iniciar Sesión</h1>

            <form method="POST" action="{{url("/login")}}">
                {!! csrf_field() !!}
                <div class="col-md-12 raw-margin-top-24">
                    <label>Correo Electrónico</label>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>Contraseña</label>
                    <input class="form-control" type="password" name="password" id="password">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>
                        Recordarme <input type="checkbox" name="remember">
                    </label>
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <a class="btn btn-default pull-left" href="{{url('/password/reset')}}">Olvidé mi contraseña</a>
                    <button class="btn btn-primary pull-right" type="submit">Iniciar Sesión</button>
                </div>

                <div class="col-md-12 raw-margin-top-24">
                    <a class="btn raw100 btn-info" href="{{url('/register')}}">Registrar</a>
                </div>
            </form>

        </div>
    </div>

@stop

