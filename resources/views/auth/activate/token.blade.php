@extends('layouts.master')

@section('app-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">

            <h1 class="text-center">Activación de tu cuenta</h1>

            <p>Hemos enviado un nuevo token de activación a tu correo electrónico.</p>
        </div>
    </div>

@stop

