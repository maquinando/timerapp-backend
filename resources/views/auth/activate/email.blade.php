@extends('layouts.master')

@section('app-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">

            <h1 class="text-center">Activa de tu cuenta</h1>

            <p>Por favor revisa tu correo electrónico para confirmar tu contraseña.</p>

            <a class="btn btn-primary" href="{{ url('activate/send-token') }}">Solicitar un nuevo token</a>

        </div>
    </div>

@stop

