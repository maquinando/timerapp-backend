@extends('layouts.master')

@section('app-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <h1 class="text-center">Registrarme</h1>

            <form method="POST" action="{{url('/register')}}">
                {!! csrf_field() !!}

                <div class="col-md-12 raw-margin-top-24">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>Correo</label>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>Contraseña</label>
                    <input class="form-control" type="password" name="password">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>Confirmar Contraseña</label>
                    <input class="form-control" type="password" name="password_confirmation">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <a class="btn btn-default pull-left" href="{{url('/login')}}">Login</a>
                    <button class="btn btn-primary pull-right" type="submit">Registrar</button>
                </div>
            </form>

        </div>
    </div>

@stop