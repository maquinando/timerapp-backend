@extends('layouts.master')

@section('app-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <h1 class="text-center">Olvidé mi contraseña</h1>

            <form method="POST" action="{{url('/password/email')}}">
                {!! csrf_field() !!}
                @include('partials.errors')
                @include('partials.status')
                <div class="col-md-12 pull-left">
                    <label>Correo Electrónico</label>
                    <input class="form-control" type="email" name="email" placeholder="Correo Electrónico" value="{{ old('email') }}">
                </div>
                <div class="col-md-12 pull-left raw-margin-top-24">
                    <a class="btn btn-default pull-left" href="{{url('/login')}}">Espera, ya la recordé!</a>
                    <button class="btn btn-primary pull-right" type="submit" class="button">Enviar enlace de recuperación de contraseña</button>
                </div>
            </form>

        </div>
    </div>

@stop
