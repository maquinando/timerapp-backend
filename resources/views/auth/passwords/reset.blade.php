@extends('layouts.master')

@section('app-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <h1 class="text-center">Reiniciar Contraseña</h1>

            <form method="POST" action="{{url('/password/reset')}}">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="col-md-12 raw-margin-top-24">
                    <label>Correo Electrónico</label>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>Contraseña</label>
                    <input class="form-control" type="password" name="password">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <label>Confirmar Contraseña</label>
                    <input class="form-control" type="password" name="password_confirmation">
                </div>
                <div class="col-md-12 raw-margin-top-24">
                    <button class="btn btn-primary" type="submit">Reiniciar Contraseña</button>
                </div>
            </form>
        </div>
    </div>

@stop