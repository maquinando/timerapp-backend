@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Edit'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'recipes.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Recipes: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('recipes.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {!! Form::model($recipe, [
                 'route' => ['recipes.update', $recipe->id], 
                 'method' => 'patch',
                 'files' => true 
            ]) !!} 
            <?php /**/
                $columns = FormMaker::getTableColumns('recipes');
                unset($columns['preview']);
                unset($columns['content']);
                unset($columns['location_id']);
            /**/ ?>
             @form_maker_object($recipe, $columns) 

            {{--  @input_maker_create('image', [
                'type' => 'relationship',
                'model' => 'App\Models\Image',
                'label' => 'Vista previa',
                'value' => 'image'
            ], $recipe);  --}}
            <div class="form-group">
                {!! Form::label('Ubicación') !!}
                <?php 
                    $locations = \App\Models\Location::all();
                ?>
                <select class="selectpicker form-control" data-live-search="true" name="location_id" value="{{ @$recipe->location->id }}">
                    @foreach ($locations as $location)
                        <option data-tokens="{{ $location->code }}" value="{{ $location->id }}">
                            {{ $location->name }}
                        </option>
                    @endforeach
                </select>
                <div class="links-wrapper clearfix">
                    <a class="pull-left" href="{!! url('/admin/locations') !!}">
                        Ver Ubicaciones
                    </a>
                    <a class="pull-right" href="{!! url('/admin/locations/create') !!}">
                        Crear Ubicación
                    </a>
                </div>
            </div>
            <div class="form-group">
                <?php 
                    $id_categories = array();
                    foreach ($recipe->categories as $category) {
                        array_push($id_categories, $category->id);
                    }
                    $categories = \App\Models\Category::all();
                ?> 
                {!! Form::label('Categorías') !!}
                <select id="category-selector" class="selectpicker form-control" data-live-search="true" name="categories[]"  multiple>
                    @foreach ($categories as $category)
                        <option data-tokens="{{ $category->id }}" value="{{ $category->id }}" <?php echo in_array($category->id, $id_categories)? 'selected':''?> >
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
                <div class="links-wrapper clearfix">
                    <a class="pull-left" href="{!! url('/admin/categories') !!}">
                        Ver Categorías
                    </a>
                    <a class="pull-right" href="{!! url('/admin/categories/create') !!}">
                        Crear Categoría
                    </a>
                </div>
            </div>
            <div class="form-group clearfix">
                
                {!! Form::label('Imagen de la receta') !!}
                @if ($recipe->preview !== null)
                <div class="preview-wrapper pull-left" style="margin-right: 15px; padding: 3px; border: solid 1px #dedede">
                    <img src="{{url('/')}}/display-image/{{$recipe->preview}}/thumbnail"  width="100"/>
                </div>
                @endif
                {!! Form::file('image', null) !!}
            </div>
            

            {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

    <div class="preparations-wrapper">
            <div class="col-md-12 clearfix">
                <h3 class="pull-left">Preparaciones</h3>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('preparations.create', array('recipe' => $recipe->id)) !!}"><i class="fa fa-plus" aria-hidden="true"></i>Agregar</a>
            </div>
            <div class="col-md-12">
                @if ($recipe->preparations->isEmpty())
                    <div class="col-md-12 raw-margin-bottom-24">
                        <div class="well text-center">No se encuentran preparaciones.</div>
                    </div>
                @else
                    <table class="table table-striped">
                        <thead>
                            <th>Name</th>
                            <th class="text-right">Action</th>
                        </thead>
                        <tbody>
                            @foreach($recipe->preparations as $preparation)
                                <tr>
                                    <td>{{ $preparation->name }}</td>
                                    <td class="text-right">
                                        
                                        <a class="btn btn-default btn-xs" href="{{ url('admin/preparations/'.$preparation->id.'/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
                                        
                                        
                                        <a class="btn btn-danger btn-xs" href="{{ url('admin/preparations/'.$preparation->id.'/remove') }}" onclick="return confirm('Estas seguro de querer eliminar esta preparación?')">Remove</a>
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                
            </div>
    </div>
@stop
