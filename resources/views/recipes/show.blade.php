@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Show'])

@section('content')

    <div class="container">
        <div class="row">
            <!-- Card Projects -->
            <div class="col-md-6 col-md-offset-3">
                <div class="card">
                    <div class="card-image">
                        <img class="img-responsive" src="http://material-design.storage.googleapis.com/publish/v_2/material_ext_publish/0Bx4BSt6jniD7TDlCYzRROE84YWM/materialdesign_introduction.png">
                        <span class="card-title">Material Cards</span>
                    </div>
                    
                    <div class="card-content">
                        <p>Cards for display in portfolio style material design by Google.</p>
                    </div>
                    
                    <div class="card-action">
                        <a href="./edit" target="new_blank" class="pull-right">Editar</a>
                    </div>
                </div>
            </div>
        </div>
</div>

@stop