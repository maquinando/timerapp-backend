@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Index'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'recipes.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Recipes</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('recipes.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if ($recipes->isEmpty())
                <div class="well text-center">No recipes found.</div>
            @else
                <table class="table table-striped">
                    <thead>
                        <th>Preview</th>
                        <th>Name</th>
                        <th>Duration</th>
                        <th class="text-right" width="200px">Action</th>
                    </thead>
                    <tbody>
                        @foreach($recipes as $recipe)
                            <tr>
                                <td>
                                    @if ($recipe->preview !== null)
                                    <a style="padding: 3px; border: solid 1px #dedede; display: inline-block;">
                                        <img src="{{url('/')}}/display-image/{{$recipe->preview}}/thumbnail" width="60" />
                                    </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! route('recipes.edit', [$recipe->id]) !!}">{{ $recipe->name }}</a>
                                </td>
                                <td>
                                    <a href="{!! route('recipes.edit', [$recipe->id]) !!}">{{ $recipe->preparation_time }} min</a>
                                </td>
                                <td class="text-right">
                                    <form method="post" action="{!! route('recipes.destroy', [$recipe->id]) !!}">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
                                        <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this recipe?')"><i class="fa fa-trash"></i> Delete</button>
                                    </form>
                                    <a class="btn btn-default btn-xs pull-right raw-margin-right-16" href="{!! route('recipes.edit', [$recipe->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $recipes; !!}
        </div>
    </div>

@stop