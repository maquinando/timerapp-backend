@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Edit'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'preparations.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Preparations: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('preparations.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {!! Form::model($preparation, ['route' => ['preparations.update', $preparation->id], 'method' => 'patch']) !!}
            <?php 
            $columns = FormMaker::getTableColumns('preparations');
            $allRecipes = \App\Models\Recipe::all();
            unset($columns['recipe_id']);
            ?>
            @form_maker_object($preparation, $columns)
            {!! Form::label('Receta') !!}
            <select id="recipe-selector" class="selectpicker form-control" data-live-search="true" name="recipe_id" >
                @foreach ($allRecipes as $recipe)
                    <option data-tokens="{{ $recipe->id }}" value="{{ $recipe->id }}" <?php echo ($preparation->recipe->id == $recipe->id)? 'selected':''?> >
                        {{ $recipe->name }}
                    </option>
                @endforeach
            </select>



            {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>
    <div class="ingredients-wrapper">
            <div class="col-md-12 clearfix">
                <h3 class="pull-left">Ingredientes</h3>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('ingredients.create', array('preparation' => $preparation->id)) !!}"><i class="fa fa-plus" aria-hidden="true"></i>Agregar</a>
            </div>
            <div class="col-md-12">
                @if ($preparation->ingredients->isEmpty())
                    <div class="col-md-12 raw-margin-bottom-24">
                        <div class="well text-center">No se encuentran ingredientes.</div>
                    </div>
                @else
                    <table class="table table-striped">
                        <thead>
                            <th>Name</th>
                            <th>Quantity</th>
                            <th class="text-right">Action</th>
                        </thead>
                        <tbody>
                            @foreach($preparation->ingredients as $ingredient)
                                <tr>
                                    <td>{{ $ingredient->name }}</td>
                                    <td>{{ $ingredient->quantity }} {{ $ingredient->units->name }}</td>
                                    <td class="text-right">
                                        
                                        <a class="btn btn-default btn-xs" href="{{ url('admin/ingredients/'.$ingredient->id.'/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
                                        
                                        
                                        <a class="btn btn-danger btn-xs" href="{{ url('admin/ingredients/'.$ingredient->id.'/remove') }}" onclick="return confirm('Estas seguro de querer eliminar este ingrediente?')">Remove</a>
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                
            </div>
    </div>
    <div class="steps-wrapper">
            <div class="col-md-12 clearfix">
                <h3 class="pull-left">Pasos</h3>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('steps.create', array('preparation' => $preparation->id)) !!}"><i class="fa fa-plus" aria-hidden="true"></i>Agregar</a>
            </div>
            <div class="col-md-12">
                @if ($preparation->steps->isEmpty())
                    <div class="col-md-12 raw-margin-bottom-24">
                        <div class="well text-center">No se encuentran pasos.</div>
                    </div>
                @else
                    <table class="table table-striped">
                        <thead>
                            <th style="">Name</th>
                            <th style="">Tiempo<br>Cocción</th>
                            <th class="text-right">Action</th>
                        </thead>
                        <tbody>
                            @foreach($preparation->steps as $step)
                                <tr>
                                    <td>{{ $step->content }}</td>
                                    <td>{{ empty($step->duration)? 0:$step->duration }} min</td>
                                    <td class="text-right">
                                        
                                        <a class="btn btn-default btn-xs" href="{{ url('admin/steps/'.$step->id.'/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
                                        
                                        
                                        <a class="btn btn-danger btn-xs" href="{{ url('admin/steps/'.$step->id.'/remove') }}" onclick="return confirm('Estas seguro de querer eliminar este paso?')">Remove</a>
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
                
            </div>
    </div>
@stop
