@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Create'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'preparations.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Preparations: Create</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['route' => 'preparations.store']) !!}
            <?php 
            $columns = FormMaker::getTableColumns('preparations');
            $recipe_id = @$_GET['recipe'] || '';
            $allRecipes = \App\Models\Recipe::all();
            unset($columns['recipe_id']);
            ?>
            @form_maker_object(null, $columns)
            {!! Form::label('Receta') !!}
            <select id="recipe-selector" class="selectpicker form-control" data-live-search="true" name="recipe_id">
                @foreach ($allRecipes as $recipe)
                    <option data-tokens="{{ $recipe->id }}" value="{{ $recipe->id }}" <?php echo ($recipe_id == $recipe->id)? 'selected':''?> >
                        {{ $recipe->name }}
                    </option>
                @endforeach
            </select>

            {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@stop