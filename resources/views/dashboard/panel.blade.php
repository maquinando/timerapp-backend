<ul class="nav nav-sidebar">
    <li @if(Request::is('dashboard', 'dashboard/*')) class="active" @endif>
        <a href="{!! url('dashboard') !!}"><span class="fa fa-dashboard"></span> Tablero</a>
    </li>
    <li @if(Request::is('user/settings', 'user/password')) class="active" @endif>
        <a href="{!! url('user/settings') !!}"><span class="fa fa-user"></span> Configuración</a>
    </li>
    <li @if(Request::is('teams', 'teams/*')) class="active" @endif>
        <a href="{!! url('teams') !!}"><span class="fa fa-users"></span> Equipos</a>
    </li>
    @if (Gate::allows('admin'))
        <li class="sidebar-header"><span>Admin</span></li>
        <li @if(Request::is('admin/users', 'admin/users/*')) class="active" @endif>
            <a href="{!! url('admin/users') !!}"><span class="fa fa-user"></span> Usuarios</a>
        </li>
        <li @if(Request::is('admin/roles', 'admin/roles/*')) class="active" @endif>
            <a href="{!! url('admin/roles') !!}"><span class="fa fa-lock"></span> Roles</a>
        </li>
        <li @if(Request::is('admin/devices', 'admin/devices/*')) class="active" @endif>
            <a href="{!! url('admin/devices') !!}"><span class="fa fa-phone"></span> Dispositivos</a>
        </li>
        <li @if(Request::is('admin/filemanager/admin')) class="active" @endif>
            <a href="{!! url('admin/filemanager/admin') !!}"><span class="fa fa-files-o"></span> Archivos</a>
        </li>
        <li @if(Request::is('admin/recipes', 'admin/recipes/*')) class="active" @endif>
            <a href="{!! url('admin/recipes') !!}"> 
                <span class="fa"><img src="{{URL::asset('/img/chef-hat.svg')}}" alt="chef hat" height="20" width="20"style="height: 1em; width: auto;"></span> Recetas
            </a>
        </li>
        <li @if(Request::is('admin/categories', 'admin/categories/*')) class="active" @endif>
            <a href="{!! url('admin/categories') !!}"><span class="fa fa-filter"></span> Categorías</a>
        </li>
        <li @if(Request::is('admin/locations', 'admin/locations/*')) class="active" @endif>
            <a href="{!! url('admin/locations') !!}"><span class="fa fa-globe"></span> Ubicación Regional</a>
        </li>
    @endif
</ul>