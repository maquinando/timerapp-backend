@extends('dashboard', ['pageTitle' => '_camelUpper_casePlural_ &raquo; Edit'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'steps.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Steps: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('steps.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            {!! Form::model($step, ['route' => ['steps.update', $step->id], 'method' => 'patch']) !!}

            <?php 
            $columns = FormMaker::getTableColumns('steps');
            unset($columns['preparation_id']);

            $allPreparations = \App\Models\Preparation::all();

             ?>
            @form_maker_object($step, $columns)

            {!! Form::label('Preparación') !!}
            <select id="recipe-selector" class="selectpicker form-control" data-live-search="true" name="preparation_id" >
                @foreach ($allPreparations as $preparation)
                    <option data-tokens="{{ $preparation->id }}" value="{{ $preparation->id }}" <?php echo ($step->preparation->id == $preparation->id)? 'selected':''?> >
                        {{ $preparation->name }}
                    </option>
                @endforeach
            </select>

            {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@stop
