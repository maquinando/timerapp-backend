<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

        <title>App</title>

        <link rel="icon" type="image/ico" href="">

        <!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <!-- Local -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/raw.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .NFI-wrapper {
                font-family: arial;
                font-size: 12px;
                -webkit-box-shadow: 0px 1px 0px #fff, 0px -1px 0px rgba(0,0,0,.1);
                -moz-box-shadow: 0px 1px 0px #fff, 0px -1px 0px rgba(0,0,0,.1);
                box-shadow: 0px 1px 0px #fff, 0px -1px 0px rgba(0,0,0,.1); 
                -moz-border-radius: 4px; 
                -webkit-border-radius: 4px;
                border-radius: 4px;
                display: block !important;
            }

            .NFI-wrapper .NFI-button {
                -moz-border-radius-topleft: 3px; 
                -moz-border-radius-bottomleft: 3px;
                -webkit-border-top-left-radius: 3px;
                -webkit-border-bottom-left-radius: 3px;
                border-top-left-radius: 3px; 
                border-bottom-left-radius: 3px;
                background-color: #0192DD;
                background-image: linear-gradient(bottom, #1774A3 0%, #0194DD 56%);
                background-image: -o-linear-gradient(bottom, #1774A3 0%, #0194DD 56%);
                background-image: -moz-linear-gradient(bottom, #1774A3 0%, #0194DD 56%);
                background-image: -webkit-linear-gradient(bottom, #1774A3 0%, #0194DD 56%);
                background-image: -ms-linear-gradient(bottom, #1774A3 0%, #0194DD 56%);
                background-image: -webkit-gradient(
                    linear,
                    left bottom,
                    left top,
                    color-stop(0, #1774A3),
                    color-stop(0.56, #0194DD)
                );
                text-shadow: 0px -1px 0px #0172bd;
                border: solid #0172bd 1px;
                border-bottom: solid #00428d 1px;
                
                -webkit-box-shadow: inset 0px 1px 0px rgba(255,255,255,.2);
                -moz-box-shadow: inset 0px 1px 0px rgba(255,255,255,.2);
                box-shadow: inset 0px 1px 0px rgba(255,255,255,.2); 	
                
                color: #fff;
                width: 100px;
                height: 30px;
                line-height: 30px;
            }
            .NFI-wrapper .NFI-button:hover {
                background: #333;
                text-shadow: 0px -1px 0px #111;
                border: solid #000 1px;
                
            }
            .NFI-wrapper .NFI-filename {
                -moz-border-radius-topright: 3px; 
                -moz-border-radius-bottomright: 3px;
                -webkit-border-top-right-radius: 3px;
                -webkit-border-bottom-right-radius: 3px;
                border-top-right-radius: 3px; 
                border-bottom-right-radius: 3px;
                width: 200px;
                border: solid #777 1px;
                border-left: none;
                height: 30px;
                line-height: 30px;
                
                background: #fff;
                -webkit-box-shadow: inset 0px 2px 0px rgba(0,0,0,.05);
                -moz-box-shadow: inset 0px 2px 0px rgba(0,0,0,.05);
                box-shadow: inset 0px 2px 0px rgba(0,0,0,.05); 
                color: #777;
                text-shadow: 0px 1px 0px #fff;
            }

        </style>

        @yield('stylesheets')
    </head>
    <body>

        @include("layouts.navigation")

        <div class="app-wrapper container-fluid raw-margin-top-50">
            @yield("app-content")
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('partials.errors')
                    @include('partials.message')
                </div>
            </div>
        </div>

        <div class="pull-left raw100 navbar navbar-fixed-bottom">
            <div class="pull-left footer">
                <p class="raw-margin-left-20">&copy; {!! date('Y'); !!} <a href="">You</a>
                    @if (Session::get('original_user'))
                        <a class="btn btn-default pull-right btn-xs" href="{{url('/users/switch-back')}}">Return to your Login</a>
                    @endif
                </p>
            </div>
        </div>

        <script type="text/javascript">
            var _token = '{!! Session::token() !!}';
            var _url = '{!! url("/") !!}';
        </script>
        @yield("pre-javascript")
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="{{ asset('js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('assets/jquery.nicefileinput.min.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/dashboard-controller.js') }}"></script>
        @yield("javascript")
    </body>
</html>