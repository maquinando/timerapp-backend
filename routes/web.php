<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a given Closure or controller and enjoy the fresh air.
|
*/


/*
|--------------------------------------------------------------------------
| Login/ Logout/ Password
|--------------------------------------------------------------------------
*/
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/*
|--------------------------------------------------------------------------
| Registration & Activation
|--------------------------------------------------------------------------
*/
Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');

Route::get('activate/token/{token}', 'Auth\ActivateController@activate');
Route::group(['middleware' => ['auth']], function () {
    Route::get('activate', 'Auth\ActivateController@showActivate');
    Route::get('activate/send-token', 'Auth\ActivateController@sendToken');
});

/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth', 'active']], function () {
    /*
    |--------------------------------------------------------------------------
    | CKEditor Images Upload
    |--------------------------------------------------------------------------
    */
    Route::post('/upload_image', function() {
        $CKEditor = Input::get('CKEditor');
        $funcNum = Input::get('CKEditorFuncNum');
        $message = $url = '';
        if (Input::hasFile('upload')) {
            $file = Input::file('upload');
            if ($file->isValid()) {
                $filename = $file->getClientOriginalName();
                $file->move(storage_path().'/images/', $filename);
                $url = public_path() .'/images/' . $filename;
            } else {
                $message = 'An error occured while uploading the file.';
            }
        } else {
            $message = 'No file uploaded.';
        }
        return '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
    });


    /*
    |--------------------------------------------------------------------------
    | General
    |--------------------------------------------------------------------------
    */

    Route::get('/users/switch-back', 'Admin\UserController@switchUserBack');

    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::get('settings', 'SettingsController@settings');
        Route::post('settings', 'SettingsController@update');
        Route::get('password', 'PasswordController@password');
        Route::post('password', 'PasswordController@update');
    });

    /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    */

    Route::get('dashboard', 'PagesController@dashboard');

    /*
    |--------------------------------------------------------------------------
    | Team Routes
    |--------------------------------------------------------------------------
    */

    Route::get('team/{name}', 'TeamController@showByName');
    Route::resource('teams', 'TeamController', ['except' => ['show']]);
    Route::post('teams/search', 'TeamController@search');
    Route::post('teams/{id}/invite', 'TeamController@inviteMember');
    Route::get('teams/{id}/remove/{userId}', 'TeamController@removeMember');



    /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

        /*
        |--------------------------------------------------------------------------
        | Users
        |--------------------------------------------------------------------------
        */
        Route::resource('users', 'UserController', ['except' => ['create', 'show']]);
        Route::post('users/search', 'UserController@search');
        Route::get('users/search', 'UserController@index');
        Route::get('users/invite', 'UserController@getInvite');
        Route::get('users/switch/{id}', 'UserController@switchToUser');
        Route::post('users/invite', 'UserController@postInvite');

        /*
        |--------------------------------------------------------------------------
        | Roles
        |--------------------------------------------------------------------------
        */
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
        Route::post('roles/search', 'RoleController@search');
        Route::get('roles/search', 'RoleController@index');

    });
    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        /*
        |--------------------------------------------------------------------------
        | Recipe Routes
        |--------------------------------------------------------------------------
        */

        Route::resource('recipes', 'RecipesController', ['except' => ['show']]);
        Route::get('team/{slug}', 'RecipesController@showBySlug');
        Route::post('recipes/search', [
            'as' => 'recipes.search',
            'uses' => 'RecipesController@search'
            ]);

        /*
        |--------------------------------------------------------------------------
        | Device Routes
        |--------------------------------------------------------------------------
        */

        Route::resource('devices', 'DevicesController', ['except' => ['show']]);
        Route::post('devices/search', [
            'as' => 'devices.search',
            'uses' => 'DevicesController@search'
            ]);



        
        /*
        |--------------------------------------------------------------------------
        | File Manager
        |--------------------------------------------------------------------------
        */ 

        Route::group(['prefix' => 'filemanager','middleware' => 'auth'], function() {    
            Route::get('admin', 'FilemanagerLaravelController@getAdmin');
            Route::get('show', 'FilemanagerLaravelController@getShow');
            Route::post('connectors', 'FilemanagerLaravelController@postConnectors');
        });



        /*
        |--------------------------------------------------------------------------
        | Preparation Routes
        |--------------------------------------------------------------------------
        */

        Route::resource('preparations', 'PreparationsController', ['except' => ['show']]);
        Route::post('preparations/search', [
            'as' => 'preparations.search',
            'uses' => 'PreparationsController@search'
        ]);

        /*
        |--------------------------------------------------------------------------
        | Ingredient Routes
        |--------------------------------------------------------------------------
        */

        Route::resource('ingredients', 'IngredientsController', ['except' => ['show']]);
        Route::post('ingredients/search', [
            'as' => 'ingredients.search',
            'uses' => 'IngredientsController@search'
        ]);


        /*
        |--------------------------------------------------------------------------
        | Category Routes
        |--------------------------------------------------------------------------
        */

        Route::resource('categories', 'CategoriesController', ['except' => ['show']]);
        Route::post('categories/search', [
            'as' => 'categories.search',
            'uses' => 'CategoriesController@search'
        ]);

        /*
        |--------------------------------------------------------------------------
        | Step Routes
        |--------------------------------------------------------------------------
        */

        Route::resource('steps', 'StepsController', ['except' => ['show']]);
        Route::post('steps/search', [
            'as' => 'steps.search',
            'uses' => 'StepsController@search'
        ]);
        /*
        |-------------------------------------------------------------
        | Location Routes
        |-------------------------------------------------------------
        */

        Route::resource('locations', 'LocationsController', ['except' => ['show']]);
        Route::post('locations/search', [
            'as' => 'locations.search',
            'uses' => 'LocationsController@search'
        ]);

        
        
    });
});
Route::get('admin/filemanager/connectors', 'FilemanagerLaravelController@getConnectors');

Route::group(['middleware' => 'cacheable'], function () {
    /*
    |--------------------------------------------------------------------------
    | Welcome Page
    |--------------------------------------------------------------------------
    */

    Route::get('/', 'PagesController@home');

    
    

    /*
    |--------------------------------------------------------------------------
    | Image Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('images', 'ImagesController', ['except' => ['show']]);
    Route::post('images/search', [
        'as' => 'images.search',
        'uses' => 'ImagesController@search'
    ]);


    route::get('display-image/{image_id}/{format?}', 'PagesController@showImage');

});




