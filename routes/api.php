<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cacheable'], function () {
    Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::post('recover', 'AuthController@recover');
        Route::post('social/login/{provider}', 'AuthController@socialLogin');

        Route::resource('recipes', 'RecipesController', ['only' => [
            'index', 'show'
            ]]);
        Route::get('filters', 'RecipesController@getFilters');

        Route::group(['middleware' => 'jwt.auth'], function () {
            Route::get('refresh', 'AuthController@refresh');
            Route::group(['prefix' => 'user'], function () {
                Route::get('profile', 'UserController@getProfile');
                Route::post('profile', 'UserController@postProfile');
            });

            Route::resource('recipes', 'RecipesController', ['except' => [
                'index', 'show'
                ]]);
            Route::resource('devices', 'DevicesController');
        });
    });




    /*
    |--------------------------------------------------------------------------
    | Preparation API Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::resource('v1/images', 'Api\ImagesController', ['as' => 'api']);
        Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
        Route::resource('v1/ingredients', 'Api\IngredientsController', ['as' => 'api']);
    });
});





/*
|--------------------------------------------------------------------------
| Category API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/categories', 'Api\CategoriesController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Location API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/locations', 'Api\LocationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Preparation API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/preparations', 'Api\PreparationsController', ['as' => 'api']);
});

/*
|--------------------------------------------------------------------------
| Step API Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('v1/steps', 'Api\StepsController', ['as' => 'api']);
});
