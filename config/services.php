<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],


    'stripe' => [
        'model'  => App\Models\UserMeta::class,
        'key'    => env('STRIPE_PUBLIC'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'github' => [
        'client_id' => '0a0abf135163a0bacfb5',
        'client_secret' => 'a05ba681eb2f01c4217cd1638036c83b46e096d9',
        'redirect' => env('APP_URL').'/auth/github/callback',
        'scopes' => ['user:email'],
    ],
    'facebook' => [
        'client_id' => '159754064538400',
        'client_secret' => '7ee109cc979f14da7bb1a1695a553f61',
        'redirect' => env('APP_URL').'/auth/facebook/callback',
        'scopes' => ['user:email'],
    ],
];
