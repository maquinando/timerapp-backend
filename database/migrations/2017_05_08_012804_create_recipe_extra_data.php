<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeExtraData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function ($table) {
            $table->increments('id');
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images');    
            $table->string('code');
            $table->string('name');
        });
        Schema::create('images', function ($table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
        });
        Schema::table('recipes', function ($table) {
            $table->string('video')->nullable();
            $table->integer('preview')->unsigned()->nullable();
            $table->foreign('preview')->references('id')->on('images');
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations');
        });

        Schema::create('recipe_gallery', function ($table) {
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images');
            $table->integer('recipe_id')->unsigned()->nullable();
            $table->foreign('recipe_id')->references('id')->on('recipes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
