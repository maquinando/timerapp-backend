<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 128)->index()->nullable();
            $table->string('name');
            $table->string('portions');
            $table->integer('calories')->unsigned()->default(0);
            $table->integer('preparation_time')->unsigned()->nullable();
            $table->text('content');
            $table->timestamps();
        });
        Schema::create('preparations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug', 128)->index()->nullable();
            $table->text('content');
            $table->timestamps();

            $table->integer('recipe_id')->unsigned();
            $table->foreign('recipe_id')->references('id')->on('recipes');
        });
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128)->index()->nullable();
            $table->string('display_name');
        });
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug', 128)->index();

            $table->integer('quantity')->unsigned();
            $table->integer('units_id')->unsigned()->nullable();
            $table->foreign('units_id')->references('id')->on('units');

            $table->integer('preparation_id')->unsigned();
            $table->foreign('preparation_id')->references('id')->on('preparations');

            $table->timestamps();
        });
        Schema::create('steps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->integer('duration')->unsigned()->nullable();
            $table->integer('weight')->unsigned()->nullable();

            $table->integer('preparation_id')->unsigned()->nullable();
            $table->foreign('preparation_id')->references('id')->on('preparations');

            $table->timestamps();
        });
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 128)->index();
            $table->string('name');
        });
        Schema::create('recipe_category', function (Blueprint $table) {
            $table->integer('recipe_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('recipe_id')->references('id')->on('recipes');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipe_steps');
        Schema::drop('recipes');
        Schema::drop('step');
    }
}
