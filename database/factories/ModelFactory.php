<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/*
|--------------------------------------------------------------------------
| UserMeta Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\UserMeta::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'phone' => $faker->phoneNumber,
        'marketing' => 1,
        'terms_and_cond' => 1,
    ];
});

$factory->define(App\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => 'member',
        'label' => 'Member',
    ];
});

/*
|--------------------------------------------------------------------------
| Team Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Team::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'name' => $faker->name
    ];
});
/*
|--------------------------------------------------------------------------
| Notification Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Notification::class, function (Faker\Generator $faker) {
    return [
        'id' => 1,
        'user_id' => 1,
        'flag' => 'info',
        'uuid' => 'lksjdflaskhdf',
        'title' => 'Testing',
        'details' => 'Your car has been impounded!',
        'is_read' => 0,
    ];
});

/*
|--------------------------------------------------------------------------
| Recipe Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Recipe::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',
		'content' => 'laravel',
		'created_at' => '2017-03-08 09:05:06',
		'updated_at' => '2017-03-08 09:05:06',


    ];
});

/*
|--------------------------------------------------------------------------
| Recipe Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Recipe::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',
		'content' => 'laravel',
		'created_at' => '2017-03-08 09:05:42',
		'updated_at' => '2017-03-08 09:05:42',


    ];
});

/*
|--------------------------------------------------------------------------
| Device Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Device::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'user_id' => '1',
		'token' => 'laravel',
		'name' => 'laravel',
		'platforms' => 'laravel',
		'deleted_at' => '2017-03-08 09:06:32',
		'created_at' => '2017-03-08 09:06:32',
		'updated_at' => '2017-03-08 09:06:32',


    ];
});

/*
|--------------------------------------------------------------------------
| Image Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Image::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'image' => 'laravel',
		'thumbnail' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:19:58',
		'updated_at' => '2017-08-08 05:19:58',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:21:46',
		'updated_at' => '2017-08-08 05:21:46',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:22:19',
		'updated_at' => '2017-08-08 05:22:19',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Ingredient Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Ingredient::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',


    ];
});

/*
|--------------------------------------------------------------------------
| Category Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Location Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'code' => 'laravel',
		'name' => 'laravel',
		'image_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:26:49',
		'updated_at' => '2017-08-31 04:26:49',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:32:21',
		'updated_at' => '2017-08-31 04:32:21',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:33:23',
		'updated_at' => '2017-08-31 04:33:23',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:34:21',
		'updated_at' => '2017-08-31 04:34:21',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:35:52',
		'updated_at' => '2017-08-31 04:35:52',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:37:25',
		'updated_at' => '2017-08-31 04:37:25',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Preparation Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Preparation::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-31 04:39:13',
		'updated_at' => '2017-08-31 04:39:13',
		'recipe_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Step Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Step::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'content' => 'laravel',
		'duration' => '1',
		'weight' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-09-02 04:26:58',
		'updated_at' => '2017-09-02 04:26:58',


    ];
});
