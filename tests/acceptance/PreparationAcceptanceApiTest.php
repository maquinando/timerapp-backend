<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PreparationAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->preparation = factory(App\Models\Preparation::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:19:58',
		'updated_at' => '2017-08-08 05:19:58',
		'recipe_id' => '1',

        ]);
        $this->preparationEdited = factory(App\Models\Preparation::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:19:58',
		'updated_at' => '2017-08-08 05:19:58',
		'recipe_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/preparations');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/preparations', $this->preparation->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/preparations', $this->preparation->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/preparations/1', $this->preparationEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('preparations', $this->preparationEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/preparations', $this->preparation->toArray());
        $response = $this->call('DELETE', 'api/v1/preparations/'.$this->preparation->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'preparation was deleted']);
    }

}
