<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ImageAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->image = factory(App\Models\Image::class)->make([
            'id' => '1',
		'image' => 'laravel',
		'thumbnail' => 'laravel',

        ]);
        $this->imageEdited = factory(App\Models\Image::class)->make([
            'id' => '1',
		'image' => 'laravel',
		'thumbnail' => 'laravel',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'images');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('images');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'images/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'images', $this->image->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('images/'.$this->image->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'images', $this->image->toArray());

        $response = $this->actor->call('GET', '/images/'.$this->image->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('image');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'images', $this->image->toArray());
        $response = $this->actor->call('PATCH', 'images/1', $this->imageEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('images', $this->imageEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'images', $this->image->toArray());

        $response = $this->call('DELETE', 'images/'.$this->image->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('images');
    }

}
