<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IngredientAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->ingredient = factory(App\Models\Ingredient::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',

        ]);
        $this->ingredientEdited = factory(App\Models\Ingredient::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'ingredients');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('ingredients');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'ingredients/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'ingredients', $this->ingredient->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('ingredients/'.$this->ingredient->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'ingredients', $this->ingredient->toArray());

        $response = $this->actor->call('GET', '/ingredients/'.$this->ingredient->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('ingredient');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'ingredients', $this->ingredient->toArray());
        $response = $this->actor->call('PATCH', 'ingredients/1', $this->ingredientEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('ingredients', $this->ingredientEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'ingredients', $this->ingredient->toArray());

        $response = $this->call('DELETE', 'ingredients/'.$this->ingredient->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('ingredients');
    }

}
