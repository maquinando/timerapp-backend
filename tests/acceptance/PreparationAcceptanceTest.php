<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PreparationAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->preparation = factory(App\Models\Preparation::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:19:58',
		'updated_at' => '2017-08-08 05:19:58',
		'recipe_id' => '1',

        ]);
        $this->preparationEdited = factory(App\Models\Preparation::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'content' => 'I am Batman',
		'created_at' => '2017-08-08 05:19:58',
		'updated_at' => '2017-08-08 05:19:58',
		'recipe_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'preparations');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('preparations');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'preparations/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'preparations', $this->preparation->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('preparations/'.$this->preparation->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'preparations', $this->preparation->toArray());

        $response = $this->actor->call('GET', '/preparations/'.$this->preparation->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('preparation');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'preparations', $this->preparation->toArray());
        $response = $this->actor->call('PATCH', 'preparations/1', $this->preparationEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('preparations', $this->preparationEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'preparations', $this->preparation->toArray());

        $response = $this->call('DELETE', 'preparations/'.$this->preparation->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('preparations');
    }

}
