<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IngredientAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->ingredient = factory(App\Models\Ingredient::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',

        ]);
        $this->ingredientEdited = factory(App\Models\Ingredient::class)->make([
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/ingredients');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/ingredients', $this->ingredient->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/ingredients', $this->ingredient->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/ingredients/1', $this->ingredientEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('ingredients', $this->ingredientEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/ingredients', $this->ingredient->toArray());
        $response = $this->call('DELETE', 'api/v1/ingredients/'.$this->ingredient->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'ingredient was deleted']);
    }

}
