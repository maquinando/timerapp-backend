<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RecipeAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->recipe = factory(App\Models\Recipe::class)->make([
            'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',
		'content' => 'laravel',
		'created_at' => '2017-03-08 09:05:42',
		'updated_at' => '2017-03-08 09:05:42',

        ]);
        $this->recipeEdited = factory(App\Models\Recipe::class)->make([
            'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',
		'content' => 'laravel',
		'created_at' => '2017-03-08 09:05:42',
		'updated_at' => '2017-03-08 09:05:42',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/recipes');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/recipes', $this->recipe->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/recipes', $this->recipe->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/recipes/1', $this->recipeEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('recipes', $this->recipeEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/recipes', $this->recipe->toArray());
        $response = $this->call('DELETE', 'api/v1/recipes/'.$this->recipe->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'recipe was deleted']);
    }

}
