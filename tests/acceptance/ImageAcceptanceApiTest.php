<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ImageAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->image = factory(App\Models\Image::class)->make([
            'id' => '1',
		'image' => 'laravel',
		'thumbnail' => 'laravel',

        ]);
        $this->imageEdited = factory(App\Models\Image::class)->make([
            'id' => '1',
		'image' => 'laravel',
		'thumbnail' => 'laravel',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/images');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/images', $this->image->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/images', $this->image->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/images/1', $this->imageEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('images', $this->imageEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/images', $this->image->toArray());
        $response = $this->call('DELETE', 'api/v1/images/'.$this->image->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'image was deleted']);
    }

}
