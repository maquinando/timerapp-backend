<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StepAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->step = factory(App\Models\Step::class)->make([
            'id' => '1',
		'content' => 'laravel',
		'duration' => '1',
		'weight' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-09-02 04:26:58',
		'updated_at' => '2017-09-02 04:26:58',

        ]);
        $this->stepEdited = factory(App\Models\Step::class)->make([
            'id' => '1',
		'content' => 'laravel',
		'duration' => '1',
		'weight' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-09-02 04:26:58',
		'updated_at' => '2017-09-02 04:26:58',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'steps');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('steps');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'steps/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'steps', $this->step->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('steps/'.$this->step->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'steps', $this->step->toArray());

        $response = $this->actor->call('GET', '/steps/'.$this->step->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('step');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'steps', $this->step->toArray());
        $response = $this->actor->call('PATCH', 'steps/1', $this->stepEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('steps', $this->stepEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'steps', $this->step->toArray());

        $response = $this->call('DELETE', 'steps/'.$this->step->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('steps');
    }

}
