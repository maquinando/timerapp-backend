<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RecipeAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->recipe = factory(App\Models\Recipe::class)->make([
            'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',
		'content' => 'laravel',
		'created_at' => '2017-03-08 09:05:42',
		'updated_at' => '2017-03-08 09:05:42',

        ]);
        $this->recipeEdited = factory(App\Models\Recipe::class)->make([
            'id' => '1',
		'slug' => 'laravel',
		'name' => 'laravel',
		'content' => 'laravel',
		'created_at' => '2017-03-08 09:05:42',
		'updated_at' => '2017-03-08 09:05:42',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'recipes');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('recipes');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'recipes/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'recipes', $this->recipe->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('recipes/'.$this->recipe->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'recipes', $this->recipe->toArray());

        $response = $this->actor->call('GET', '/recipes/'.$this->recipe->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('recipe');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'recipes', $this->recipe->toArray());
        $response = $this->actor->call('PATCH', 'recipes/1', $this->recipeEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('recipes', $this->recipeEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'recipes', $this->recipe->toArray());

        $response = $this->call('DELETE', 'recipes/'.$this->recipe->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('recipes');
    }

}
