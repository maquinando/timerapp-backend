<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DeviceAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->device = factory(App\Models\Device::class)->make([
            'id' => '1',
		'user_id' => '1',
		'token' => 'laravel',
		'name' => 'laravel',
		'platforms' => 'laravel',
		'deleted_at' => '2017-03-08 09:06:32',
		'created_at' => '2017-03-08 09:06:32',
		'updated_at' => '2017-03-08 09:06:32',

        ]);
        $this->deviceEdited = factory(App\Models\Device::class)->make([
            'id' => '1',
		'user_id' => '1',
		'token' => 'laravel',
		'name' => 'laravel',
		'platforms' => 'laravel',
		'deleted_at' => '2017-03-08 09:06:32',
		'created_at' => '2017-03-08 09:06:32',
		'updated_at' => '2017-03-08 09:06:32',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/devices');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/devices', $this->device->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/devices', $this->device->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/devices/1', $this->deviceEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('devices', $this->deviceEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/devices', $this->device->toArray());
        $response = $this->call('DELETE', 'api/v1/devices/'.$this->device->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'device was deleted']);
    }

}
