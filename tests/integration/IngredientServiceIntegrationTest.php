<?php

use App\Services\IngredientService;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IngredientServiceIntegrationTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->service = $this->app->make(IngredientService::class);
        $this->originalArray = [
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',

        ];
        $this->editedArray = [
            'id' => '1',
		'name' => 'laravel',
		'slug' => 'laravel',
		'quantity' => '1',
		'units_id' => '1',
		'preparation_id' => '1',
		'created_at' => '2017-08-08 05:23:22',
		'updated_at' => '2017-08-08 05:23:22',

        ];
        $this->searchTerm = '';
    }

    public function testAll()
    {
        $response = $this->service->all();
        $this->assertEquals(get_class($response), 'Illuminate\Database\Eloquent\Collection');
        $this->assertTrue(is_array($response->toArray()));
        $this->assertEquals(0, count($response->toArray()));
    }

    public function testPaginated()
    {
        $response = $this->service->paginated(25);
        $this->assertEquals(get_class($response), 'Illuminate\Pagination\LengthAwarePaginator');
        $this->assertEquals(0, $response->total());
    }

    public function testSearch()
    {
        $response = $this->service->search($this->searchTerm, 25);
        $this->assertEquals(get_class($response), 'Illuminate\Pagination\LengthAwarePaginator');
        $this->assertEquals(0, $response->total());
    }

    public function testCreate()
    {
        $response = $this->service->create($this->originalArray);
        $this->assertEquals(get_class($response), 'App\Models\Ingredient');
        $this->assertEquals(1, $response->id);
    }

    public function testFind()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->find($item->id);
        $this->assertEquals(1, $response->id);
    }

    public function testUpdate()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->update($item->id, $this->editedArray);

        $this->assertEquals(1, $response->id);
        $this->seeInDatabase('ingredients', $this->editedArray);
    }

    public function testDestroy()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->destroy($item->id);
        $this->assertTrue($response);
    }

}

